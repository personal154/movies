//
//  String+Extensions.swift
//  FindMovie
//
//  Created by André Medeiros da Silva on 19/08/20.
//  Copyright © 2020 André Medeiros da Silva. All rights reserved.
//

import Foundation

extension String {
    var localized: String {
          return Bundle.main.localizedString(forKey: self, value: nil, table: "StandardLocalizations")
    }
        
    func localizeWithFormat(arguments: CVarArg...) -> String{
        return String(format: self.localized, arguments: arguments)
    }
    
    func dataStringToYear() -> String {
        let dataArray = self.split{$0 == "-"}.map(String.init)
        return dataArray[0]
    }
}

