//
//  UIView+Extensions.swift
//  FindMovie
//
//  Created by André Medeiros da Silva on 22/08/20.
//  Copyright © 2020 André Medeiros da Silva. All rights reserved.
//

import Foundation
import Lottie

extension UIView {
    func playStarAnimation() {
         let checkMarkAnimation =  AnimationView(name: "star")
         self.contentMode = .center
         self.addSubview(checkMarkAnimation)
         checkMarkAnimation.frame = self.bounds
         checkMarkAnimation.loopMode = .playOnce
         checkMarkAnimation.play()
    }
}


