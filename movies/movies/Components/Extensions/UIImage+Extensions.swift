//
//  UIImage+extensions.swift
//  FindMovie
//
//  Created by André Medeiros da Silva on 15/08/20.
//  Copyright © 2020 André Medeiros da Silva. All rights reserved.
//
import Foundation
import UIKit

enum imageSize: Int{
    case w92 = 92
    case w154 = 154
    case w185 = 185
    case w342 = 342
    case w500 = 500
    case w780 = 780
}


extension UIImageView {
    
    func load(url: String?, size: imageSize) {
        guard let urlString = url else {
            return
        }
        
        let urlConverted = URL(string: "https://image.tmdb.org/t/p/w\(size.rawValue)\(urlString)")
        guard let urlOk = urlConverted else {
            return
        }
        
        DispatchQueue.global().async { [weak self] in
            if let data = try? Data(contentsOf: urlOk) {
                if let image = UIImage(data: data) {
                    DispatchQueue.main.async {
                        self?.image = image
                    }
                }
            }
        }
    }
}
