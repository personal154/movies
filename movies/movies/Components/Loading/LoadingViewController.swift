//
//  LoadingViewController.swift
//  FindMovie
//
//  Created by André Medeiros da Silva on 17/08/20.
//  Copyright © 2020 André Medeiros da Silva. All rights reserved.
//

import UIKit
import SwiftGifOrigin

class LoadingViewController: UIViewController {

    @IBOutlet weak var loadingImage: UIImageView!
    override func viewDidLoad() {
        super.viewDidLoad()
       self.loadingImage.loadGif(asset: "star")
    }
}
