//
//  GeneralErrorViewController.swift
//  FindMovie
//
//  Created by André Medeiros da Silva on 15/08/20.
//  Copyright © 2020 André Medeiros da Silva. All rights reserved.
//

import UIKit
import Foundation

protocol GeneralErrorDelegate: class {
    func buttonAction()
}

class GeneralErrorViewController: UIViewController {

    @IBOutlet weak var retryButton: UIButton!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    var text: String?
    var desc: String?
    
    weak var generalErrorDelegate: GeneralErrorDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupMessage(title: text ?? String(), description: desc ?? String())
        setupButton()
    }
    
    @IBAction func tapOnRetryButton(_ sender: Any) {
        self.generalErrorDelegate?.buttonAction()
    }
    
    func setupButton() {
        retryButton.layer.borderWidth = 2.0
        retryButton.layer.borderColor = UIColor(red: 0, green: 178, blue: 228, alpha: 1.0).cgColor
        retryButton.layer.cornerRadius = 18.0
        retryButton.clipsToBounds = true
    }
    
    func setupMessage(title: String, description: String) {
        self.titleLabel.text = title
        self.descriptionLabel.text = description
        
    }
}
