//
//  MovieDetailsViewController.swift
//  FindMovie
//
//  Created by André Medeiros da Silva on 19/08/20.
//  Copyright © 2020 André Medeiros da Silva. All rights reserved.
//

import UIKit

class MovieDetailsViewController: UIViewController {

    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var posterImage: UIImageView!
    @IBOutlet weak var backButton: UIImageView!
    @IBOutlet weak var releaseDate: UILabel!
    @IBOutlet weak var filmSelectImage: UIImageView!
    @IBOutlet weak var animationView: UIView!
    @IBOutlet weak var favStarIcon: UIImageView!
    
    var poster: String?
    var desc: String?
    var name: String?
    var movieDate: String?
    private var isClicked: Bool = false
    
    override func viewDidLoad() {
          super.viewDidLoad()
        self.setup(poster: poster ?? String(), description: desc ?? String(), title: name ?? String(), releaseDate: movieDate ?? String())
    }
    
    func setup(poster: String, description: String, title: String, releaseDate: String) {
        self.descriptionLabel.text = description
        self.titleLabel.text = title
        self.posterImage.load(url: poster, size: .w500)
        guard let date = movieDate else { return }
        self.releaseDate.text = "release: \(String(describing: date.dataStringToYear()))"
        setupBackButton()
        setupfavoriteButton()
    }
    
    func setupBackButton() {
        let tap = UITapGestureRecognizer(target: self, action: #selector(tapOnBackButton))
        backButton.addGestureRecognizer(tap)
        backButton.isUserInteractionEnabled = true
    }
    
    func setupfavoriteButton() {
        let tap = UITapGestureRecognizer(target: self, action: #selector(tapOnFavorite))
        filmSelectImage.addGestureRecognizer(tap)
        filmSelectImage.isUserInteractionEnabled = true
    }
    
    @objc func tapOnBackButton() {
        dismiss(animated: true, completion: nil)
    }
    
    func changeFavoriteImage() {
        if isClicked {
            favStarIcon.image = UIImage(named:"fav_star_clicked")
        } else {
            favStarIcon.image = UIImage(named:"fav_star")
        }
    }
    
    func setFavoriteClick() {
        self.isClicked = !isClicked
    }
    
    @objc func tapOnFavorite() {
        setFavoriteClick()
        animationView.playStarAnimation()
        changeFavoriteImage()
    }
}
