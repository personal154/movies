//
//  MoviesListCollectionViewCell.swift
//  FindMovie
//
//  Created by André Medeiros da Silva on 13/08/20.
//  Copyright © 2020 André Medeiros da Silva. All rights reserved.
//

import UIKit

class MoviesListCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var collectionViewLabel: UILabel!
    
    @IBOutlet weak var collectionViewImage: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setupLabel()
    }
    
    func setupLabel() {
        collectionViewLabel.lineBreakMode = NSLineBreakMode.byWordWrapping
        collectionViewLabel.numberOfLines = 2
    }
    
    func setupCells(response: Movies.MoviesCall.Result) {
        collectionViewLabel.text = response.title
        collectionViewImage.load(url: response.poster_path, size: .w185)
    }

}
