//
//  MovieListPresenter.swift
//  FindMovie
//
//  Created by André Medeiros da Silva on 06/08/20.
//  Copyright (c) 2020 André Medeiros da Silva. All rights reserved.
//
//  This file was generated by the Clean Swift Xcode Templates so
//  you can apply clean architecture to your iOS and Mac projects,
//  see http://clean-swift.com
//

import UIKit

protocol MovieListPresentationLogic {
    func presentMoviesList(page: Int, response: Movies.MoviesCall.Response)
}

protocol MovieListPresentationOutput: AnyObject {
    func displayMoviesList(page: Int, viewModel: Movies.MoviesCall.Response)
}

class MovieListPresenter: MovieListPresentationLogic {
  weak var output: MovieListPresentationOutput?
  weak var viewController: MovieListDisplayLogic?
    
  // MARK: Do something
  func presentMoviesList(page: Int, response: Movies.MoviesCall.Response) {
   // var viewModel = ViewModel()
      output?.displayMoviesList(page: page, viewModel: response)
  }
  
  
}


