//
//  Constants.swift
//  FindMovie
//
//  Created by André Medeiros da Silva on 06/08/20.
//  Copyright © 2020 André Medeiros da Silva. All rights reserved.
//

import UIKit

class Constants: NSObject {
    
    let API_KEY = "?api_key=dff797f2148a0b107a9c9f4c74a6a8d3"
    let REQUEST_MOVIES_LIST = "https://api.themoviedb.org/3/movie/"
}
